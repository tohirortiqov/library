package com.example.library.controller;

import com.example.library.dto.CreateBookDto;
import com.example.library.entity.Books;
import com.example.library.service.BookService;
import javassist.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    public List<Books> getAllBooks(){
        return bookService.getAllBooks();
    }

    @PostMapping
    public Books addBook(@RequestBody CreateBookDto dto){

        return bookService.save(dto);
    }

    @DeleteMapping
    public void deleteBook(@PathVariable Long bookId){
        bookService.delete(bookId);
    }

    @PutMapping("/{bookId}")
    public Books updateBook(@PathVariable Long bookId,@RequestBody CreateBookDto bookDto) throws NotFoundException {
        return bookService.updateBook(bookId,bookDto);
    }


}
