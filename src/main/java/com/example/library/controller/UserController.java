package com.example.library.controller;

import com.example.library.dto.CreateUserDto;
import com.example.library.entity.Users;
import com.example.library.service.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<Users> getAllUsers(){
        return userService.getAllUsers();
    }

    @PostMapping("/author")
    public Users createAuthor(@RequestBody CreateUserDto dto){

        return userService.createAuthor(dto);
    }

    @PostMapping("/reader")
    public Users createReader(@RequestBody CreateUserDto dto){
        return userService.createReader(dto);
    }

    @PutMapping("/{userId}")
    public Users updateUser(@PathVariable Long userId,@RequestBody Users user) throws NotFoundException {
        return userService.updateUser(user);
    }

    @DeleteMapping
    public void deleteUser(@PathVariable Long userId){
        userService.deleteUser(userId);
    }
}
