package com.example.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
@Data
@Entity
@Table(name = "books")
public class Books extends BaseEntity {

    @Column
    private String name;
    @Column
    private String type;
    @Column
    private String author;
    @Column
    private int year;
}
