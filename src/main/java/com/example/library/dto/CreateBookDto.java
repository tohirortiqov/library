package com.example.library.dto;

import lombok.Data;

@Data
public class CreateBookDto {

    private String name;
    private String type;
    private String author;
    private int year;
}
