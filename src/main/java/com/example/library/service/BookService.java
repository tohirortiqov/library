package com.example.library.service;

import com.example.library.dto.CreateBookDto;
import com.example.library.entity.Books;
import javassist.NotFoundException;

import java.util.List;

public interface BookService {
    public List<Books> getAllBooks();

    Books save(CreateBookDto dto);

    void delete(Long bookId);

    Books updateBook(Long bookId, CreateBookDto bookDto) throws NotFoundException;
}
