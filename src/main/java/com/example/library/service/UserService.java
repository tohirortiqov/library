package com.example.library.service;

import com.example.library.dto.CreateUserDto;
import com.example.library.entity.Users;
import javassist.NotFoundException;

import java.util.List;

public interface UserService {

    List<Users> getAllUsers();

    Users createAuthor(CreateUserDto dto);

    Users createReader(CreateUserDto dto);

    void deleteUser(Long user);

    Users updateUser(Long userId,CreateUserDto userDto) throws NotFoundException;
}
