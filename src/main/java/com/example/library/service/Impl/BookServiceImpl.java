package com.example.library.service.Impl;

import com.example.library.dto.CreateBookDto;
import com.example.library.entity.Books;
import com.example.library.repository.BookRepository;
import com.example.library.service.BookService;
import javassist.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public List<Books> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Books save(CreateBookDto dto) {
        Books book= new Books();
        BeanUtils.copyProperties(book,dto);
        book.setCreatedAt(LocalDateTime.now());
        bookRepository.save(book);
        return book;
    }

    @Override
    public void delete(Long bookId) {
        bookRepository.deleteById(bookId);
    }

    @Override
    public Books updateBook(Long bookId, CreateBookDto bookDto) throws NotFoundException {
        Optional<Books> book =bookRepository.findById(bookId);
        if (!book.isPresent()){
            throw new NotFoundException("This book does not exist in the database.");
        }
        book.get().setAuthor(bookDto.getAuthor());
        book.get().setName(bookDto.getName());
        book.get().setType(bookDto.getType());
        book.get().setYear(bookDto.getYear());
        bookRepository.save(book.get());
        book.get().setUpdateAt(LocalDateTime.now());
        return book.get();
    }


}
