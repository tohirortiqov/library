package com.example.library.service.Impl;

import com.example.library.dto.CreateUserDto;
import com.example.library.entity.RoleType;
import com.example.library.entity.Users;
import com.example.library.repository.UserRepository;
import com.example.library.service.UserService;
import javassist.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Users> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Users createAuthor(CreateUserDto dto) {
        Users user=new Users();
        BeanUtils.copyProperties(dto,user);
        user.setRole(RoleType.AUTHORS);
        user.setCreatedAt(LocalDateTime.now());
        userRepository.save(user);
        return user;
    }

    @Override
    public Users createReader(CreateUserDto dto) {
        Users user=new Users();
        BeanUtils.copyProperties(dto,user);
        user.setRole(RoleType.READER);
        user.setCreatedAt(LocalDateTime.now());
        userRepository.save(user);
        return user;
    }

    @Override
    public Users updateUser(Long userId,CreateUserDto userDto) throws NotFoundException {
        Optional<Users> user=userRepository.findById(userId);
        if (!user.isPresent()){
            throw new NotFoundException("The user is not found in the database.");
        }
        user.get().setFirstName(userDto.getFirstName());
        user.get().setLastName(userDto.getLastName());
        user.get().setPhone(userDto.getPhone());
        user.get().setUpdateAt(LocalDateTime.now());
        return user.get();
    }

    @Override
    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }


}
